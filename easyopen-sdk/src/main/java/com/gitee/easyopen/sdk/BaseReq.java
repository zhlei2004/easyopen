package com.gitee.easyopen.sdk;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class BaseReq {

    private static final String FORMAT_JSON = "json";
    private static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private String name;
    private String version = "";
    private Object data;
    private String timestamp = new SimpleDateFormat(TIME_FORMAT).format(new Date());
    private String access_token = "";

    public BaseReq(String name, Object data) {
        this.name = name;
        this.data = data;
    }

    public BaseReq(String name, String version, Object data) {
        this.name = name;
        this.version = version;
        this.data = data;
    }

    public abstract Class<?> buildRespClass();

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getFormat() {
        return FORMAT_JSON;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

}
