package com.gitee.easyopen.sdk;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.easyopen.sdk.util.ClassUtil;

public abstract class BaseResp<T> {
    private String code;
    private String msg;
    private String data;

    private Class<T> clazz;

    @SuppressWarnings("unchecked")
    public BaseResp() {
        this.clazz = (Class<T>) ClassUtil.getSuperClassGenricType(getClass(), 0);
    }

    @JSONField(serialize = false)
    public T getBody() {
        if(clazz == String.class) {
            return (T)data;
        }
        return JSON.parseObject(data, clazz);
    }

    public boolean isSuccess() {
        return "0".equals(code);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
