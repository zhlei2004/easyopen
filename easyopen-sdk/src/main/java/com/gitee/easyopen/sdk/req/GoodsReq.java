package com.gitee.easyopen.sdk.req;

import com.gitee.easyopen.sdk.BaseReq;
import com.gitee.easyopen.sdk.resp.GoodsResp;

public class GoodsReq extends BaseReq {

    public GoodsReq(String name, Object data) {
        super(name, data);
    }

    public GoodsReq(String name, String version, Object data) {
        super(name, version, data);
    }

    @Override
    public Class<?> buildRespClass() {
        return GoodsResp.class;
    }

}
