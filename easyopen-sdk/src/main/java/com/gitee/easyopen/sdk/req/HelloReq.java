package com.gitee.easyopen.sdk.req;

import com.gitee.easyopen.sdk.BaseNoParamReq;
import com.gitee.easyopen.sdk.resp.HelloResp;

public class HelloReq extends BaseNoParamReq {

    public HelloReq(String name) {
        super(name);
    }

    @Override
    public Class<?> buildRespClass() {
        return HelloResp.class;
    }

}