package com.gitee.easyopen.sdk;

import org.junit.Test;

import com.gitee.easyopen.sdk.req.HelloReq;
import com.gitee.easyopen.sdk.resp.HelloResp;

import junit.framework.TestCase;

public class HelloTest extends TestCase {

    String url = "http://localhost:8080/api";
    String appKey = "test";
    String secret = "123456";
    // 创建一个实例即可
    OpenClient client = new OpenClient(url, appKey, secret);

    @Test
    public void testGet() throws Exception {
        HelloReq req = new HelloReq("hello"); // hello对应@Api中的name属性，即接口名称

        HelloResp result = client.request(req); // 发送请求
        if (result.isSuccess()) {
            String resp = result.getBody();
            System.out.println(resp); // 返回hello world
        } else {
            throw new RuntimeException(result.getMsg());
        }

    }

}