package com.gitee.easyopen.sdk;

import java.net.URLEncoder;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gitee.easyopen.sdk.modal.Goods;
import com.gitee.easyopen.sdk.param.GoodsParam;
import com.gitee.easyopen.sdk.req.GoodsReq;
import com.gitee.easyopen.sdk.resp.GoodsResp;
import com.gitee.easyopen.sdk.util.PostUtil;

import junit.framework.TestCase;

public class SdkTest extends TestCase {

    String url = "http://localhost:8080/api";
    String appKey = "test";
    String secret = "123456";

    OpenClient client = new OpenClient(url, appKey, secret);
    OpenClient client_en = new OpenClient(url, appKey, secret, "en");

    @Test
    public void testGet() throws Exception {
        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("goods.get", param);

        GoodsResp result = client.request(req);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            System.out.println("errorMsg:" + result.getMsg());
        }
        System.out.println("--------------------");
    }
    
    @Test
    public void testGet2() throws Exception {

        GoodsParam param = new GoodsParam();
        //param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("goods.get", param);

        GoodsResp result = client_en.request(req);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            System.out.println("errorMsg:" + result.getMsg());
        }
        System.out.println("--------------------");
    }
    
    // 先浏览器访问http://localhost:8080/go_oauth2 获取accessToken
    @Test
    public void testGetToken() throws Exception {
        String accessToken = "b94b87ee95829243d46bb15dee8bca90";

        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        
        GoodsReq req = new GoodsReq("user.goods.get", param);
        
        GoodsResp result = client_en.request(req, accessToken);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            throw new RuntimeException(result.getMsg());
        }
        System.out.println("--------------------");

    }
    
    // 先浏览器访问http://localhost:8080/jwtLogin 获取jwt
    @Test
    public void testGetWithJwt() throws Exception {
        String jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIyIiwiZXhwIjoxNTE5NzA1NjgwLCJpYXQiOjE1MTk2OTg0ODAsInVzZXJuYW1lIjoiamltIn0.Sk35GtxRGURvrKL0VhvqGHaArKuNaDJ4F1NRXSuNmgw";

        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        
        GoodsReq req = new GoodsReq("userjwt.goods.get", param);
        
        GoodsResp result = client_en.requestWithJwt(req, jwt);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            throw new RuntimeException(result.getMsg());
        }
        System.out.println("--------------------");

    }
    
    @Test
    public void testAdd() throws Exception {
        JSONObject param = new JSONObject();

        GoodsParam goods = new GoodsParam();
        goods.setGoods_name("333333333333333333333333333333333333");

        param.put("name", "goods.add");
        param.put("data", URLEncoder.encode(JSON.toJSONString(goods), "UTF-8"));
        param.put("version", "");

        System.out.println("--------------------");
        System.out.println("请求内容:" + JSON.toJSONString(param));

        String resp = PostUtil.post(url, param,"zh",null);

        System.out.println(resp);
        System.out.println("--------------------");
    }

}
