package com.gitee.easyopen.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyopenSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasyopenSpringbootApplication.class, args);
	}
}
