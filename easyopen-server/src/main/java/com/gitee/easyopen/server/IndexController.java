package com.gitee.easyopen.server;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gitee.easyopen.ApiConfig;
import com.gitee.easyopen.support.ApiController;

@Controller
@RequestMapping(value = "/api")
public class IndexController extends ApiController {

    @Override
    protected void initApiConfig(ApiConfig apiConfig) {
        apiConfig.setShowDoc(true); // 显示文档页面
        // 配置国际化消息
        apiConfig.getIsvModules().add("i18n/isv/goods_error");

        // 配置秘钥键值对
        Map<String, String> appSecretStore = new HashMap<String, String>();
        appSecretStore.put("test", "123456");
        
        apiConfig.addAppSecret(appSecretStore);
    }

}
