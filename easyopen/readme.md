# easyopen-core

开放平台核心代码。jar包已发布到中央仓库，可直接依赖：

```
<dependency>
    <groupId>net.oschina.durcframework</groupId>
    <artifactId>easyopen</artifactId>
    <version>1.0.3</version>
</dependency>
```