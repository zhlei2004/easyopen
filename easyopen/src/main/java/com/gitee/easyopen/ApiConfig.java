package com.gitee.easyopen;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.gitee.easyopen.auth.Oauth2Manager;
import com.gitee.easyopen.auth.Oauth2Service;
import com.gitee.easyopen.auth.impl.Oauth2ServiceImpl;
import com.gitee.easyopen.jwt.JwtService;
import com.gitee.easyopen.jwt.impl.JwtServiceImpl;
import com.gitee.easyopen.serializer.JsonResultSerializer;
import com.gitee.easyopen.serializer.XmlResultSerializer;

/**
 * 配置类,所有配置相关都在这里.
 * 
 * @author tanghc
 *
 */
public class ApiConfig {
    
    /**
     * app秘钥管理
     */
    private AppSecretManager appSecretManager = new CacheAppSecretManager();
    /**
     * 校验接口
     */
    private Validator validator = new ApiValidator(this);
    /**
     * 返回结果
     */
    private ResultCreator resultCreator = new ApiResultCreator();
    /**
     * json序列化
     */
    private ResultSerializer jsonResultSerializer = new JsonResultSerializer();
    /**
     * xml序列化
     */
    private ResultSerializer xmlResultSerializer = new XmlResultSerializer();

    private RespWriter respWriter = new ApiRespWriter();

    private JwtService jwtService = new JwtServiceImpl(this);
    
    private Invoker invoker;

    private Oauth2Service oauth2Service;

    private Oauth2Manager oauth2Manager;

    /**
     * 错误模块
     */
    private List<String> isvModules = new ArrayList<String>();
    {
        isvModules.add("i18n/isv/error");
    };

    /**
     * 超时时间
     */
    private int timeoutSeconds = 3;

    /**
     * 是否生成doc文档
     */
    private boolean showDoc;

    /**
     * 忽略验证
     */
    private boolean ignoreValidate;

    /**
     * 登录视图页面用于，mvc视图，如：loginView
     */
    private String oauth2LoginUri = "/oauth2login";

    /**
     * oauth2的accessToken过期时间,单位秒,默认2小时
     */
    private long oauth2ExpireIn = 7200;

    /**
     * jwt过期时间,秒,默认2小时
     */
    private int jwtExpireIn = 7200;

    public ApiConfig() {
        this.invoker = new ApiInvoker(this);
    }

    public Oauth2Service initOauth2Service(Oauth2Manager oauth2Manager) {
        this.oauth2Manager = oauth2Manager;
        oauth2Service = new Oauth2ServiceImpl(oauth2Manager);
        return oauth2Service;
    }

    public boolean isShowDoc() {
        return showDoc;
    }

    public void setShowDoc(boolean showDoc) {
        this.showDoc = showDoc;
    }

    /**
     * 添加秘钥配置，map中存放秘钥信息，key对应appKey，value对应secret
     * 
     * @param appSecretStore
     */
    public void addAppSecret(Map<String, String> appSecretStore) {
        this.appSecretManager.addAppSecret(appSecretStore);
    }

    public AppSecretManager getAppSecretManager() {
        return appSecretManager;
    }

    public void setAppSecretManager(AppSecretManager appSecretManager) {
        this.appSecretManager = appSecretManager;
    }

    public ResultCreator getResultCreator() {
        return resultCreator;
    }

    public void setResultCreator(ResultCreator resultCreator) {
        this.resultCreator = resultCreator;
    }

    public int getTimeoutSeconds() {
        return timeoutSeconds;
    }

    public void setTimeoutSeconds(int timeoutSeconds) {
        this.timeoutSeconds = timeoutSeconds;
    }

    public ResultSerializer getJsonResultSerializer() {
        return jsonResultSerializer;
    }

    public void setJsonResultSerializer(ResultSerializer jsonResultSerializer) {
        this.jsonResultSerializer = jsonResultSerializer;
    }

    public ResultSerializer getXmlResultSerializer() {
        return xmlResultSerializer;
    }

    public void setXmlResultSerializer(ResultSerializer xmlResultSerializer) {
        this.xmlResultSerializer = xmlResultSerializer;
    }

    public List<String> getIsvModules() {
        return isvModules;
    }

    public void setIsvModules(List<String> isvModules) {
        this.isvModules = isvModules;
    }

    public boolean isIgnoreValidate() {
        return ignoreValidate;
    }

    public void setIgnoreValidate(boolean ignoreValidate) {
        this.ignoreValidate = ignoreValidate;
    }

    public RespWriter getRespWriter() {
        return respWriter;
    }

    public void setRespWriter(RespWriter respWriter) {
        this.respWriter = respWriter;
    }

    public String getOauth2LoginUri() {
        return oauth2LoginUri;
    }

    public void setOauth2LoginUri(String oauth2LoginUri) {
        this.oauth2LoginUri = oauth2LoginUri;
    }

    public long getOauth2ExpireIn() {
        return oauth2ExpireIn;
    }

    public void setOauth2ExpireIn(long oauth2ExpireIn) {
        this.oauth2ExpireIn = oauth2ExpireIn;
    }

    public Oauth2Service getOauth2Service() {
        return oauth2Service;
    }

    public void setOauth2Service(Oauth2Service oauth2Service) {
        this.oauth2Service = oauth2Service;
    }

    public Oauth2Manager getOauth2Manager() {
        return oauth2Manager;
    }

    public void setOauth2Manager(Oauth2Manager oauth2Manager) {
        this.oauth2Manager = oauth2Manager;
    }

    public int getJwtExpireIn() {
        return jwtExpireIn;
    }

    public void setJwtExpireIn(int jwtExpireIn) {
        this.jwtExpireIn = jwtExpireIn;
    }

    public JwtService getJwtService() {
        return jwtService;
    }

    public void setJwtService(JwtService jwtService) {
        jwtService.setApiConfig(this);
        this.jwtService = jwtService;
    }
    

    public Validator getValidator() {
        return validator;
    }

    public void setValidator(Validator validator) {
        validator.setApiConfig(this);
        this.validator = validator;
    }
    
    public Invoker getInvoker() {
        return invoker;
    }

    public void setInvoker(Invoker invoker) {
        invoker.setApiConfig(this);
        this.invoker = invoker;
    }

    // =====================================
    public void setApiName(String apiName) {
        ParamNames.API_NAME = apiName;
    }

    public void setVersionName(String versionName) {
        ParamNames.VERSION_NAME = versionName;
    }

    public void setAppKeyName(String appKeyName) {
        ParamNames.APP_KEY_NAME = appKeyName;
    }

    public void setDataName(String dataName) {
        ParamNames.DATA_NAME = dataName;
    }

    public void setTimestampName(String timestampName) {
        ParamNames.TIMESTAMP_NAME = timestampName;
    }

    public void setSignName(String signName) {
        ParamNames.SIGN_NAME = signName;
    }

}
