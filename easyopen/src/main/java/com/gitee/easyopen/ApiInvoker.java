package com.gitee.easyopen;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.gitee.easyopen.auth.Oauth2Manager;
import com.gitee.easyopen.auth.OpenUser;
import com.gitee.easyopen.bean.ApiDefinition;
import com.gitee.easyopen.bean.DefinitionHolder;
import com.gitee.easyopen.exception.ApiException;
import com.gitee.easyopen.jwt.JwtService;
import com.gitee.easyopen.message.Errors;
import com.gitee.easyopen.util.ApiUtil;

/**
 * 处理客户端请求分发
 * 
 * @author tanghc
 *
 */
public class ApiInvoker implements Invoker {

    private static final Logger logger = LoggerFactory.getLogger(ApiInvoker.class);
    
    private static final String UTF8 = "UTF-8";
    private static final String FORMAT_JSON = "json";
    private static final String FORMAT_XML = "xml";
    private static final String AUTHORIZATION = "Authorization";
    private static final String PREFIX_BEARER = "Bearer ";
    
    
    private ApiConfig apiConfig; // 配置项
    private Validator validator; // 负责校验
    private ResultCreator resultCreator; // 负责生成最终结果

    private ResultSerializer jsonFormatter; // 负责把最终结果格式化成json
    private ResultSerializer xmlFormatter; // 负责把最终结果格式化成xml
    
    private RespWriter respWriter;
    private JwtService jwtService;
    
    public ApiInvoker() {
        super();
    }

    public ApiInvoker(ApiConfig apiConfig) {
        super();
        this.apiConfig = apiConfig;
        this.init(apiConfig);
    }
    
    private void init(ApiConfig apiConfig) {
        this.validator = apiConfig.getValidator();
        this.resultCreator = apiConfig.getResultCreator();

        this.jsonFormatter = apiConfig.getJsonResultSerializer();
        this.xmlFormatter = apiConfig.getXmlResultSerializer();
        
        this.respWriter = apiConfig.getRespWriter();
        
        this.jwtService = apiConfig.getJwtService();
    }

    @Override
    public void invoke(HttpServletRequest request, HttpServletResponse response) {
        Object result = null;
        ResultCreator resultCreator = this.getResultCreator();

        ApiParam param = this.buildApiParam(request);
        ApiContext.setApiParam(param);
        this.initJwtInfo(request, param);
        try {
            logger.info("收到客户端请求,ip={},参数={}", ApiUtil.getClientIP(request), param.toJSONString());
            result = this.doInvoke(param);
        } catch (ApiException e) {
            result = resultCreator.createErrorResult(e.getCode(), e.getMessage(), e.getData());
        } catch (Throwable e) {
            result = this.caugthException(e);
        }

        this.responseResult(response, result, param.fatchFormat());
    }
    
    @Override
    public void responseResult(HttpServletResponse response, Throwable e) {
        Result result = this.caugthException(e);
        this.responseResult(response, result, FORMAT_JSON);
    }

    // 捕获异常
    private Result caugthException(Throwable e) {
        logger.error(e.getMessage(), e);
        return this.getResultCreator().createErrorResult(Errors.SYS_ERROR.getCode(), e.getMessage(), null);
    }
    
    private void initJwtInfo(HttpServletRequest request,ApiParam param) {
        String jwt = request.getHeader(AUTHORIZATION);
        if(jwt == null) {
            return;
        }
        if(jwt.startsWith(PREFIX_BEARER)) {
            jwt = jwt.replace(PREFIX_BEARER, "");
            Map<String, Claim> data = this.jwtService.verfiyJWT(jwt);
            ApiContext.setJwtData(data);
        } 
    }
    
    /**
     * 写数据到客户端
     * @param response
     * @param result 结果
     * @param format 返回类型，json,xml之一
     */
    public void responseResult(HttpServletResponse response, Object result, String format) {
        if (FORMAT_XML.equalsIgnoreCase(format)) {
            String text = this.xmlFormatter.serialize(result);
            this.respWriter.writeText(response, text);
        } else {
            String json = this.jsonFormatter.serialize(result);
            this.respWriter.writeJson(response, json);
        }
    }

    /**
     * 构建api参数
     * @param request
     * @return
     */
    protected ApiParam buildApiParam(HttpServletRequest request) {
        String requestJson = null;
        try {
            requestJson = ApiUtil.getJson(request);
        } catch (Exception e) {
            logger.error("构建系统参数失败", e);
            throw new ApiException(e);
        }
    
        if (StringUtils.isEmpty(requestJson)) {
            throw Errors.ERROR_PARAM.getException();
        }
        
        JSONObject jsonObject = null;
        try {
            jsonObject = JSONObject.parseObject(requestJson);
        }catch (Exception e) {
            throw Errors.ERROR_JSON_DATA.getException(e.getMessage());
        }

        ApiParam param = new ApiParam(jsonObject);
        param.setRequest(request);
        
        return param;
    }

    /**
     * 调用接口，返回业务结果
     * @param param
     * @return
     * @throws Throwable
     */
    protected Object doInvoke(ApiParam param) throws Throwable {
        ApiDefinition definition = DefinitionHolder.getByParam(param);
        if (definition == null) {
            throw Errors.NO_API.getException(param.fatchName(), param.fatchVersion());
        }
        param.setIgnoreSign(definition.isIgnoreSign());
        param.setIgnoreValidate(definition.isIgnoreValidate());
        
        this.getValidator().validate(param);
        this.checkAccessToken(param);

        String busiJsonData = param.fatchData(); // 业务参数json格式
        busiJsonData = URLDecoder.decode(busiJsonData, UTF8);
        // 业务参数Class
        Class<?> arguClass = definition.getMethodArguClass();

        Object methodArgu = null;

        if (arguClass != null) {
            methodArgu = JSON.parseObject(busiJsonData, arguClass);
            this.getValidator().validateBusiParam(methodArgu);
        }
        
        Object invokeResult = null;
        
        try {
            // 调用method方法
            if (methodArgu == null) {
                invokeResult = definition.getMethod().invoke(definition.getHandler());
            } else {
                invokeResult = definition.getMethod().invoke(definition.getHandler(), methodArgu);
            }
            
            if(invokeResult == null) {
                invokeResult = new EmptyObject();
            }
            
            if(definition.isWrapResult()) { // 对返回结果包装
                return resultCreator.createResult(invokeResult);
            }else{
                return invokeResult;
            }
        } catch (InvocationTargetException ex) {
            throw ex.getTargetException();
        } catch (Exception e) {
            throw e;
        }
    }
    
    /**
     * 验证accessToken
     * @param param
     * @throws Oauth2UnauthorizedException
     */
    protected void checkAccessToken(ApiParam param) {
        Oauth2Manager manager = apiConfig.getOauth2Manager();
        String accessToken = param.fatchAccessToken();
        if(StringUtils.isEmpty(accessToken)) {
            return;
        }
        try {
            OpenUser openUser = manager.getUserByAccessToken(accessToken);
            ApiContext.setAccessTokenUser(openUser);
        } catch (ApiException e) {
            throw e;
        } catch (Exception e) {
            throw new ApiException(e);
        }
    }
    
    private static class EmptyObject implements Serializable {
        private static final long serialVersionUID = 1713263598232463135L;
    }

    @Override
    public ApiConfig getApiConfig() {
        return apiConfig;
    }

    @Override
    public void setApiConfig(ApiConfig apiConfig) {
        this.apiConfig = apiConfig;
        this.init(apiConfig);
    }

    public Validator getValidator() {
        return validator;
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }

    public ResultCreator getResultCreator() {
        return resultCreator;
    }

    public void setResultCreator(ResultCreator resultCreator) {
        this.resultCreator = resultCreator;
    }

}
