package com.gitee.easyopen;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONObject;

/**
 * 客户端传来的参数放在这里.
 * 
 * @author tanghc
 *
 */
public class ApiParam extends JSONObject implements Param {
    private static final long serialVersionUID = 6718200590738465201L;
    private static final String DEFAULT_FORMAT = "json";

    public ApiParam(Map<String, Object> map) {
        super(map);
    }

    private boolean ignoreSign;
    private boolean ignoreValidate;
    
    private HttpServletRequest request;
    
    public HttpServletRequest fatchRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public boolean fatchIgnoreSign() {
        return ignoreSign;
    }

    public void setIgnoreSign(boolean ignoreSign) {
        this.ignoreSign = ignoreSign;
    }
    
    public boolean fatchIgnoreValidate() {
        return ignoreValidate;
    }

    public void setIgnoreValidate(boolean ignoreValidate) {
        this.ignoreValidate = ignoreValidate;
    }

    /**
     * 接口名,如:goods.list
     */
    @Override
    public String fatchName() {
        return getString(ParamNames.API_NAME);
    }

    public void setName(String name) {
        put(ParamNames.API_NAME, name);
    }
    
    public String fatchNameVersion() {
        if ("".equals(this.fatchVersion())) {
            return this.fatchName();
        } else {
            return this.fatchName() + " " + this.fatchVersion();
        }
    }

    /**
     * 版本号
     */
    @Override
    public String fatchVersion() {
        return getString(ParamNames.VERSION_NAME);
    }

    public void setVersion(String version) {
        put(ParamNames.VERSION_NAME, version);
    }

    /**
     * 接入应用ID
     */
    @Override
    public String fatchAppKey() {
        return getString(ParamNames.APP_KEY_NAME);
    }

    public void setAppKey(String appKey) {
        put(ParamNames.APP_KEY_NAME, appKey);
    }

    /**
     * 参数,urlencode后的
     */
    @Override
    public String fatchData() {
        return getString(ParamNames.DATA_NAME);
    }

    public void setData(String json) {
        put(ParamNames.DATA_NAME, json);
    }

    /**
     * 时间戳，格式为yyyy-MM-dd HH:mm:ss，例如：2015-01-01 12:00:00
     */
    @Override
    public String fatchTimestamp() {
        return getString(ParamNames.TIMESTAMP_NAME);
    }

    public void setTimestamp(String timestamp) {
        put(ParamNames.TIMESTAMP_NAME, timestamp);
    }

    /**
     * 签名串
     */
    @Override
    public String fatchSign() {
        return getString(ParamNames.SIGN_NAME);
    }

    public void setSign(String sign) {
        put(ParamNames.SIGN_NAME, sign);
    }

    @Override
    public String fatchFormat() {
        String format = getString(ParamNames.FORMAT_NAME);
        if (format == null) {
            return DEFAULT_FORMAT;
        }
        return format;
    }

    public void setFormat(String format) {
        put(ParamNames.FORMAT_NAME, format);
    }

    @Override
    public String fatchAccessToken() {
        return getString(ParamNames.ACCESS_TOKEN_NAME);
    }
    
}
