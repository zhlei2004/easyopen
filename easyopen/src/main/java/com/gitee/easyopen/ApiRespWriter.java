package com.gitee.easyopen;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;

public class ApiRespWriter implements RespWriter {

    private static final String UTF8 = "UTF-8";

    @Override
    public void writeJson(HttpServletResponse response, String json) {
        doWriter(response, MediaType.APPLICATION_JSON_UTF8_VALUE, json);
    }

    @Override
    public void writeText(HttpServletResponse response, String text) {
        doWriter(response, MediaType.TEXT_PLAIN_VALUE, text);
    }

    /**
     * 发送内容
     * 
     * @param response
     * @param contentType
     * @param text
     */
    public static void doWriter(HttpServletResponse response, String contentType, String text) {
        response.setContentType(contentType);
        response.setCharacterEncoding(UTF8);
        try {
            response.getWriter().write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
