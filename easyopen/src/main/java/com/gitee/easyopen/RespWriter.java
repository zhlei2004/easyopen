package com.gitee.easyopen;

import javax.servlet.http.HttpServletResponse;

/**
 * 写数据到客户端
 * 
 * @author tanghc
 *
 */
public interface RespWriter {
    /**
     * 输出json
     * 
     * @param response
     * @param json
     */
    void writeJson(HttpServletResponse response, String json);

    /**
     * 输出普通文本
     * 
     * @param response
     * @param text
     */
    void writeText(HttpServletResponse response, String text);
}
