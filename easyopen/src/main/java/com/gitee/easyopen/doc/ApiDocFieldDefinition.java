package com.gitee.easyopen.doc;

import java.util.List;

/**
 * 文档参数字段信息
 * 
 * @author tanghc
 *
 */
public class ApiDocFieldDefinition {
    private String name;
    private String dataType;
    private String required;
    private String example;
    private String description;

    private List<ApiDocFieldDefinition> elements;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    public List<ApiDocFieldDefinition> getElements() {
        return elements;
    }

    public void setElements(List<ApiDocFieldDefinition> elements) {
        this.elements = elements;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ApiDocFieldDefinition other = (ApiDocFieldDefinition) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

}
