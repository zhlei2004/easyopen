package com.gitee.easyopen.doc;

import java.util.List;

/**
 * 接口内容
 * @author tanghc
 *
 */
public class ApiDocItem implements Comparable<ApiDocItem> {

    private String name;
    private String version;
    private String description;

    private List<ApiDocFieldDefinition> paramDefinitions;
    private List<ApiDocFieldDefinition> resultDefinitions;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ApiDocFieldDefinition> getParamDefinitions() {
        return paramDefinitions;
    }

    public void setParamDefinitions(List<ApiDocFieldDefinition> paramDefinitions) {
        this.paramDefinitions = paramDefinitions;
    }

    public List<ApiDocFieldDefinition> getResultDefinitions() {
        return resultDefinitions;
    }

    public void setResultDefinitions(List<ApiDocFieldDefinition> resultDefinitions) {
        this.resultDefinitions = resultDefinitions;
    }

    @Override
    public int compareTo(ApiDocItem o) {
        if (o == null) {
            return 0;
        } else {
            return this.name.compareTo(o.name);
        }
    }

}
