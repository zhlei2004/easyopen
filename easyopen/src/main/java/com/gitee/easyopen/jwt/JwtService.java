package com.gitee.easyopen.jwt;

import java.util.Map;

import com.auth0.jwt.interfaces.Claim;
import com.gitee.easyopen.HasConfig;

public interface JwtService extends HasConfig{

    /**
     * @param apiConfig
     *            配置
     * @param data
     *            内容
     * @return 返回jwt
     * @throws Exception
     */
    String createJWT(Map<String, String> data);

    /**
     * 校验JWT字符串
     * 
     * @param token
     * @return
     * @throws Exception
     */
    Map<String, Claim> verfiyJWT(String token);
}
