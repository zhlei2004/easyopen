package com.gitee.easyopen.register;

import org.springframework.context.ApplicationContext;
import org.springframework.util.Assert;

import com.gitee.easyopen.ApiConfig;
import com.gitee.easyopen.ApiContext;

public abstract class AbstractInitializer implements Initializer {
    private static volatile boolean inited = false;

    @Override
    public void init(ApplicationContext applicationContext, ApiConfig apiConfig) {
        if (!inited) {
            Assert.notNull(applicationContext, "applicationContext不能为null");
            Assert.notNull(apiConfig, "apiConfig不能为null");
            
            new ApiRegister(apiConfig, applicationContext).regist();
            
            ApiContext.setApiConfig(apiConfig);
            inited = true;
        }
    }

}
