package com.gitee.easyopen.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;

import com.alibaba.fastjson.JSON;
import com.gitee.easyopen.ApiParam;
import com.gitee.easyopen.ParamNames;
import com.gitee.easyopen.exception.ErrorSignException;
import com.gitee.easyopen.message.Errors;

public class ApiUtil {

    private static final String CONTENT_TYPE_URLENCODED = "application/x-www-form-urlencoded";
    private static final String CONTENT_TYPE_JSON = "application/json";

    private static final String UTF8 = "UTF-8";
    private static final String MD5 = "MD5";
    private static final String ZERO = "0";

    public static void checkSign(ApiParam param, String secret) throws ErrorSignException {
        if (!isRightSign(param, secret)) {
            throw new ErrorSignException();
        }
    }

    public static boolean isRightSign(Map<String, ?> jsonObject, String secret) {
        boolean isSame = false;
        String signCode = jsonObject.get(ParamNames.SIGN_NAME).toString();

        if (signCode != null) {
            // 移除签名串
            jsonObject.remove(ParamNames.SIGN_NAME);

            String clientSign;
            try {
                clientSign = buildSign(jsonObject, secret);
                isSame = signCode.equals(clientSign);
            } catch (IOException e) {
            }

        }

        return isSame;
    }

    /**
     * 构建签名
     * 
     * @param paramsMap
     *            参数
     * @param secret
     *            密钥
     * @return
     * @throws IOException
     */
    public static String buildSign(Map<String, ?> paramsMap, String secret) throws IOException {
        Set<String> keySet = paramsMap.keySet();
        List<String> paramNames = new ArrayList<String>(keySet);

        Collections.sort(paramNames);

        StringBuilder paramNameValue = new StringBuilder();

        for (String paramName : paramNames) {
            paramNameValue.append(paramName).append(paramsMap.get(paramName));
        }

        String source = secret + paramNameValue.toString() + secret;

        return md5(source);
    }

    /**
     * 生成md5,全部大写
     * 
     * @param message
     * @return
     */
    public static String md5(String message) {
        try {
            // 1 创建一个提供信息摘要算法的对象，初始化为md5算法对象
            MessageDigest md = MessageDigest.getInstance(MD5);

            // 2 将消息变成byte数组
            byte[] input = message.getBytes();

            // 3 计算后获得字节数组,这就是那128位了
            byte[] buff = md.digest(input);

            // 4 把数组每一字节（一个字节占八位）换成16进制连成md5字符串
            return byte2hex(buff);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 二进制转十六进制字符串
     * 
     * @param bytes
     * @return
     */
    private static String byte2hex(byte[] bytes) {
        StringBuilder sign = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(bytes[i] & 0xFF);
            if (hex.length() == 1) {
                sign.append(ZERO);
            }
            sign.append(hex.toUpperCase());
        }
        return sign.toString();
    }

    /**
     * 获取json参数
     * 
     * @param request
     * @return
     * @throws IOException
     */
    public static String getJson(HttpServletRequest request) throws Exception {
        String requestJson = null;
        String contectType = request.getContentType();
        if (contectType == null || "".equals(contectType.trim())) {
            throw Errors.NO_CONTECT_TYPE_SUPPORT.getException(contectType);
        }

        contectType = contectType.toLowerCase();

        if (contectType.contains(CONTENT_TYPE_JSON)) {
            requestJson = IOUtils.toString(request.getInputStream(), UTF8);
        } else if (contectType.contains(CONTENT_TYPE_URLENCODED)) {
            Map<String, Object> params = convertRequestParamsToMap(request);
            requestJson = JSON.toJSONString(params);
        } else {
            throw Errors.NO_CONTECT_TYPE_SUPPORT.getException(contectType);
        }
        return requestJson;
    }

    /**
     * request中的参数转换成map
     * 
     * @param request
     * @return
     */
    public static Map<String, Object> convertRequestParamsToMap(HttpServletRequest request) {
        Map<String, Object> retMap = new HashMap<String, Object>();

        Set<Entry<String, String[]>> entrySet = request.getParameterMap().entrySet();

        for (Entry<String, String[]> entry : entrySet) {
            String name = entry.getKey();
            String[] values = entry.getValue();
            if (values.length == 1) {
                retMap.put(name, values[0]);
            } else if (values.length > 1) {
                retMap.put(name, values);
            } else {
                retMap.put(name, "");
            }
        }

        return retMap;
    }

    /**
     * 获取客户端真实IP
     * 
     * @param request
     * @return
     */
    public static String getClientIP(HttpServletRequest request) {
        String ipAddress = null;
        ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            if (ipAddress.equals("127.0.0.1")) {
                // 根据网卡取本机配置的IP
                InetAddress inet = null;
                try {
                    inet = InetAddress.getLocalHost();
                    ipAddress = inet.getHostAddress();
                } catch (UnknownHostException e) {
                }
            }

        }

        // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if (ipAddress != null && ipAddress.length() > 15) { // "***.***.***.***".length()
                                                            // = 15
            if (ipAddress.indexOf(",") > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
            }
        }
        return ipAddress;

    }

}
